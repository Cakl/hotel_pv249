Rails.application.routes.draw do
  resources :landmarks
  resources :services
  devise_for :users
  resources :hotels do
    resources :commentaries
  end
  root "hotels#index"
end
