class City < ActiveRecord::Base
  belongs_to :destination
  has_many :destinations
end
