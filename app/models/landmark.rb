class Landmark < ActiveRecord::Base

  has_many :hotellandmarks
  has_many :hotels, :through => :hotellandmarks

end
