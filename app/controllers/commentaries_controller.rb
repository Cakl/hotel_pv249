class CommentariesController < ApplicationController
  before_action :find_hotel
  before_action :find_commentary, only: [:edit,:update,:destroy]
  before_action :authenticate_user!, only: [:new,:edit]

  def new
    @commentary = Commentary.new
  end

  def create
    @commentary = Commentary.new(commentary_params)
    @commentary.hotel_id= @hotel.id
    @commentary.user_id = current_user.id


      if @commentary.save
        redirect_to hotel_path(@hotel)
      else
        respond_to do |format|
        format.html { redirect_to new_hotel_commentary_path(@hotel), notice: 'Comment was not successfully created.' }
      end
    end

  end

  def edit
  end

  def update
    if @commentary.update(commentary_params)
      redirect_to hotel_path(@hotel)
    else
      render 'edit'
    end
  end

  def destroy
    @commentary.destroy
    redirect_to hotel_path(@hotel)
  end

  private

  def commentary_params
    params.require(:commentary).permit(:score, :comment)
  end

  def find_hotel
    @hotel = Hotel.find(params[:hotel_id])
  end

  def find_commentary
    @commentary = Commentary.find(params[:id])
  end

end
