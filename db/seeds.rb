
Destination.create!([{name: "Australia"},
					 {name: "Canada"},
					 {name: "Czech Republic"},
					 {name: "Espana"},
					 {name: "France"},
					 {name: "Ireland"},
					 {name: "Italia"},
					 {name: "Portugal"}
                    ])

City.create!([{name: "Adelaide", destination_id: 1},
              {name: "Roxby Downs", destination_id: 1},
              {name: "Melbourne", destination_id: 1},
              {name: "Brisbane City", destination_id: 1},
              {name: "Prestonsburg", destination_id: 2},
              {name: "Pikeville", destination_id: 2},
              {name: "Inez", destination_id: 2},
              {name: "Logan", destination_id: 2},
              {name: "Olomouc", destination_id: 3},
              {name: "Praha", destination_id: 3},
              {name: "Brno", destination_id: 3},
              {name: "Ostrava", destination_id: 3}
             ])

Service.create!([{name: "Air conditioning"},
				 {name: "Coffee/tea maker"},
				 {name: "Hair dryer"},
				 {name: "LED TV"},
				 {name: "Free WiFi"},
				 {name: "Refrigerator"}
				])

Landmark.create!([{name: "Swimming pool"},
				 {name: "City centre"},
				 {name: "Airport"},
				 {name: "Train station"},
				 {name: "Cinema"},
				 {name: "Shopping centre"}
				]) 				          
