class CreateHotellandmarks < ActiveRecord::Migration
  def change
    create_table :hotellandmarks do |t|
      t.integer :hotel_id
      t.integer :landmark_id

      t.timestamps null: false
    end
  end
end
